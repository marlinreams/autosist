import React from "react";

const initialState = {
  customer_name: "",
  vin: "",
  DateTime: "",
  technician: "",
  reason: "",
};

class CreateServiceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicians;

    const technicianUrl = "http://localhost:8080/api/services/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
    }
    this.setState(initialState);
  };

  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({ customer_name: value });
  };
  handleTechnicianChange = (event) => {
    const value = event.target.value;
    this.setState({ technician: value });
  };
  handleDateTimeChange = (event) => {
    const value = event.target.value;
    this.setState({ DateTime: value });
  };
  handleVinChange = (event) => {
    const value = event.target.value;
    this.setState({ vin: value });
  };
  handleReasonChange = (event) => {
    const value = event.target.value;
    this.setState({ reason: value });
  };

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Service Request</h1>
            <form id="create-technician-form" onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleVinChange}
                  value={this.state.vin}
                  placeholder="Vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">Vin #</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.customer_name}
                  placeholder="Name"
                  required
                  type="text"
                  name="customer_name"
                  id="customer_name"
                  className="form-control"
                />
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleDateTimeChange}
                  value={this.state.DateTime}
                  placeholder="DateTime"
                  required
                  type="datetime-local"
                  name="datetime"
                  id="datetime"
                  className="form-control"
                />
                <label htmlFor="datetime">Date/Time</label>
              </div>
              <select
                onChange={this.handleTechnicianChange}
                value={this.state.technician}
                required
                id="technician"
                name="technician"
                className="form-select"
              >
                <option>Choose a Technician</option>
                {this.state.technicians?.map((tech) => {
                  return (
                    <option key={tech.number} value={tech.number}>
                      {tech.name}
                    </option>
                  );
                })}
              </select>
              <div className="mb-3">
                <label htmlFor="reason">Customer Concern</label>
                <textarea
                  onChange={this.handleReasonChange}
                  placeholder="Please write a short description of customer problem..."
                  className="form-control"
                  required
                  type="text"
                  name="reason"
                  value={this.state.reason}
                  id="reason"
                  rows="3"
                ></textarea>
              </div>
              <button className="btn btn-outline-success">
                Create Service
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateServiceForm;
