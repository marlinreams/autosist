import React from "react";

function ManufacturerList(props) {
  return (
    <div className="pt-4">
      <h1>Vehicle Manufacturers</h1>
      <table className="table table-striped">
        <thead className="table table-dark">
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {props.manufacturers?.map((manufacturer, i) => {
            return (
              <tr key={i}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
