import React from "react";

class CreateVehicleModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      pictureUrl: "",
      manufacturers: [],
      manufacturer_id: "",
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePictureChange = this.handlePictureChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  handlePictureChange(event) {
    const value = event.target.value;
    this.setState({ pictureUrl: value });
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer_id: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    data.picture_url = data.pictureUrl;
    delete data.pictureUrl;
    delete data.manufacturers;

    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const modelResponse = await fetch(modelUrl, fetchConfig);

    if (modelResponse.ok) {
      const cleared = {
        name: "",
        pictureUrl: "",
        manufacturer_id: "",
      };

      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";

    const manufacturerResponse = await fetch(manufacturerUrl);

    if (manufacturerResponse.ok) {
      const data = await manufacturerResponse.json();

      this.setState({ manufacturers: data.manufacturers });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Vehicle Model</h1>
            <form onSubmit={this.handleSubmit} id="create-manufacturer">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handlePictureChange}
                  value={this.state.picture_url}
                  placeholder="Picture Url"
                  required
                  type="text"
                  name="picture_url"
                  id="picture_url"
                  className="form-control"
                />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleManufacturerChange}
                  value={this.state.manufacturer_id}
                  required
                  name="manufacturer"
                  id="manufacturer"
                  className="form-select"
                >
                  <option value="">Choose a Manufacturer</option>
                  {this.state.manufacturers.map((manufacturer) => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-outline-success">
                Add Vehicle Model
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateVehicleModel;
