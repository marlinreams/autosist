import React from "react";

function Sales(props) {
  return (
    <div className="pt-4">
      <table className=" table table-striped">
        <thead className="table table-dark">
          <tr>
            <td>Automobile Vin</td>
            <td>Sales Person</td>
            <td>Sold To</td>
            <td>Price</td>
          </tr>
        </thead>
        <tbody>
          {props.sales?.map((sale, i) => {
            return (
              <tr key={i}>
                <td>{sale.automobile.vin}</td>
                <td>{sale.seller.name}</td>
                <td>{sale.customer.name}</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Sales;
