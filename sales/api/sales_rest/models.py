from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False, null=True)
    
    def __str__(self):
        return self.vin
    
class SalesPerson(models.Model):
    name = models.CharField(max_length=75)
    number = models.CharField(max_length=50, unique=True)
    
    def __str__(self):
        return self.name
    
class PotentialCustomer(models.Model):
    name = models.CharField(max_length=75)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=20)
    
    
    def __str__(self):
        return self.name
    
class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.PROTECT,
    )
    seller = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sales",
        on_delete=models.PROTECT,
    )
    price = models.IntegerField()
    
    def __str__(self):
        return ("Sold By: " + self.seller.name)
