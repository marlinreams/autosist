from base64 import encode
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, SalesPerson, PotentialCustomer, SalesRecord
import json
from common.json import ModelEncoder
from django.http import JsonResponse


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "number",
        "id",
    ]
    
class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "name",
        "address",
        "phone",
        "id",
    ]
    
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
        "sold",
        "id",
    ]
    
class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "seller",
        "customer",
        "price",
        "id",
    ]
    
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "seller": SalesPersonEncoder(),
        "customer": PotentialCustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":
        seller = SalesPerson.objects.all()
        return JsonResponse(
            {"sellers": seller},
            encoder=SalesPersonEncoder
        )
    else:
        content = json.loads(request.body)
        seller = SalesPerson.objects.create(**content)
        return JsonResponse(
            seller,
            encoder=SalesPersonEncoder,
            safe=False,
        )
        
@require_http_methods(["GET", "POST"])
def api_list_potential_customers(request):
    if request.method == "GET":
        potential_customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"customers": potential_customers},
            encoder=PotentialCustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        potential_customer = PotentialCustomer.objects.create(**content)
        return JsonResponse(
            potential_customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )
        
def api_show_potential_customer(request, pk):
    potential_customer = PotentialCustomer.objects.get(id=pk)
    return JsonResponse(
        potential_customer,
        encoder=PotentialCustomerEncoder,
        safe=False,
    )
    
@require_http_methods(["GET", "POST"])
def api_list_sales_record(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        
        try:
           seller_id = content["seller"]
           seller = SalesPerson.objects.get(id=seller_id)
           content["seller"] = seller
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales Person doesn't exist!"},
                status=400,
            )
            
        try:
           customer_id = content["customer"]
           customer = PotentialCustomer.objects.get(id=customer_id)
           content["customer"] = customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer doesn't exist!"},
                status=400,
            )
           
        try:
            automobile_id = content["automobile"]
            automobile = AutomobileVO.objects.get(id=automobile_id)
            automobile.sold = True
            automobile.save()
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile doesn't exist!"},
                status=400,
            )
            
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False,
        )
        
@require_http_methods(["GET", "POST"])
def api_list_autos(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )
    else:
        content = json.loads(request.body)
        auto = AutomobileVO.objects.create(**content)
        return JsonResponse(
            auto,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
        
def api_list_unsold_autos(request):
    unsold_cars = AutomobileVO.objects.filter(sold=False)
    return JsonResponse(
        {"automobiles": unsold_cars},
        encoder=AutomobileVOEncoder,
        safe=False,
    )
    
def api_show_sales_by_seller(request, pk):
    sales = SalesRecord.objects.filter(seller=pk)
    return JsonResponse(
        {"sales": sales},
        encoder=SalesRecordEncoder,
    )