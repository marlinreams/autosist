from django.contrib import admin
from .models import Service, Technician, AutomobileVO
# Register your models here.
admin.site.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass
admin.site.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass
admin.site.register(AutomobileVO)
